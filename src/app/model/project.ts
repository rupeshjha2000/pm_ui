export class Project{
    id:number
    projectName:string
    managerName:string
    managerId:number
    priority:number
    dateCheck:boolean
    startDate?:Date
    endDate?: Date 
    isActive:boolean
    isCompleted:boolean
}