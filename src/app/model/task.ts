export class Task{
    id :number
    taskName: string
    projectId: number 
    projectName: string 
    parentTask: boolean
    parentTaskId: number 
    parentTaskName: string 
    startDate?: Date 
    endDate? :Date 
    userId: number 
    userName: string 
    isCompleted: boolean
    priority: number 
}