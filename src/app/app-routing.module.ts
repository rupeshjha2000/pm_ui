import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { ProjectComponent } from './project/project.component';
import { TaskComponent } from './task/task.component';
import { ProjectmanagerComponent } from './projectmanager/projectmanager.component';
import { ViewtaskComponent } from './viewtask/viewtask.component';


const routes: Routes = [ 
  //{path:'',component:ProjectmanagerComponent},
  {path:'', component: ViewtaskComponent},
  {path:'user', component: UserComponent},
  {path:'project', component: ProjectComponent},
  {path:'task', component: TaskComponent},
  {path:'viewtask', component: ViewtaskComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
