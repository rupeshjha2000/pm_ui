import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../model/user';
import { Project } from '../model/project';
import { UserService, AlertService, ProjectService } from '../_services';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
  ascending:boolean=true;
  projectLst: Project[];
  projectLstMaster: Project[];
  searchText:string ="";
  searchManagerText:string ="";
  addUpdateText="Add";
  projectForm: FormGroup;
  userLst: User[]=[];
  userLstMaster: User[]=[];
  orderBY:string="id";
  recordCount:number=0;
 
  constructor(
    private readonly userService: UserService,
    private readonly projectService: ProjectService,
    private readonly formB: FormBuilder,
    private readonly alert: AlertService,
    private messageService: MessageService) { }

  ngOnInit() {
    this.buildForm();
    this.getAllProjects();
    this.getUsers();
  }
  buildForm() {
    var formatedDt=this.getdateFormat(new Date());
    var enddateformatted=this.getdateFormat(this.addDays(new Date(),1));

    this.projectForm = this.formB.group({
      'id': [null],
      'projectName': ['', [Validators.required, Validators.maxLength(50)]],
      'managerName': ['', [Validators.required]],
      'managerId': [null, [Validators.required]],
      'priority': [0], 
      'dateCheck': [true],
      'startDtaeDisplay':[formatedDt],
      'endDateDisplay':[enddateformatted],
       'isActive': [true]
    })
  }


  getAllProjects(){
    
    this.projectService.getAllProjects().subscribe(
      data=>{ 
        this.projectLst= data;
        this.projectLstMaster= data;
      
      }
    )
  }
  getUsers(){
    this.userService.getAll().subscribe(
      data=>{ 
        this.userLst= data;
        this.userLstMaster=data;

       if(this.searchManagerText && this.searchManagerText.trim())
        {
        this.userLst=this.userLstMaster.filter(a=>a.firstName.toLowerCase().indexOf(this.searchManagerText.toLowerCase()) >-1 || a.lastName.toLowerCase().indexOf(this.searchManagerText.toLowerCase()) >-1 || a.empId.indexOf(this.searchManagerText) >-1)
        }else
        {
          this.userLst=this.userLstMaster;
        }
      }
    )
  }
  
setManger(usr:User){
  //this.userLst=[];
  this.projectForm.get('managerName').setValue(usr.firstName + " "+ usr.lastName+" ("+usr.empId+")")
  this.projectForm.get('managerId').setValue(usr.id)
  //$('#MdlManager').modal('hide');
  
}
onSearchPManagerKeyPress(event: any){
  if(event.charCode==13 || event.charCode==10){ 
   
    this.searchManager();
  }
}
searchManager()
 {
   this.getUsers();
  
}
OnEnterKeyPressed(event: any){
  if(event.charCode==10 ||event.charCode==13){ 
    if(this.searchText && this.searchText.trim())
    {
    this.userLst=this.userLstMaster.filter(a=>a.firstName.toLowerCase().indexOf(this.searchText.toLowerCase()) >-1 || a.lastName.toLowerCase().indexOf(this.searchText.toLowerCase()) >-1 || a.empId.indexOf(this.searchText) >-1)
    }else
    {
      this.getUsers();
    }
  }
}

  showSuccess(msg : string) {
    this.messageService.add({severity:'success', summary:'Success', detail:msg,closable:true,life:3000,sticky:false});
  }
  
  showError(msg : string) {
    this.messageService.add({severity:'error', summary:'Error', detail:msg,closable:true,life:3000,sticky:false});
  }
  addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
}
  onSubmit(){
    let proj = <Project>this.projectForm.value;
    
    if(this.projectForm.controls['dateCheck'].value){
      proj.startDate = new Date(this.projectForm.controls['startDtaeDisplay'].value)
      proj.endDate = new Date(this.projectForm.controls['endDateDisplay'].value)
    }else{
      proj.startDate = null
      proj.endDate =null
    }

    //Date Checking 
    if(this.projectForm.controls['dateCheck'].value){
      if(proj.startDate>proj.endDate){
          this.showError("End Date must be greater than Start Date!!");
          return false;
      }
      
    } 
    if (!this.projectForm.controls['dateCheck'].value)
      {
       proj.startDate=new Date();
       proj.endDate=this.addDays(new Date(),1);
        //this.projectForm.value.startDate =new Date();
       //this.projectForm.value.endDate = this.addDays(new Date(),1);
      }
    this.projectService.addUpdateProject(proj).subscribe(
      data=>{
        if(data.result)
            {
            this.onReset();
            this.getAllProjects();
            this.showSuccess('Project Save successful')
            } 
            else
            {
              this.showError(data.message);
            }  
      },
      err=>{
        this.showError(err);
      }
    ) 
 }

 onEdit(proj:Project){ 
  this.addUpdateText="Edit";
  let inputField: HTMLElement = <HTMLElement>document.querySelectorAll('.projectname')[0];
      inputField && inputField.focus();
  var editObj={
    'id': proj.id,
    'projectName':proj.projectName,
    'managerName': proj.managerName,
    'managerId': proj.managerId,
    'priority': proj.priority, 
    'dateCheck': proj.dateCheck,
    'startDtaeDisplay':this.getdateFormat( new Date(proj.startDate)),
    'endDateDisplay':this.getdateFormat( new Date(proj.endDate)),
    'isActive': proj.isActive
  } 
 this.projectForm.setValue(editObj);
    if(proj.dateCheck){
      this.projectForm.get('startDtaeDisplay').enable();
      this.projectForm.get('endDateDisplay').enable();
    }else{
      this.projectForm.get('startDtaeDisplay').disable();
      this.projectForm.get('endDateDisplay').disable();
    }
 }

onDelete(proj:Project){

    this.projectService.deleteProject(proj).subscribe(
      data=>{
        if(data.result)
        {
        this.onReset();
        this.getAllProjects();
        this.showSuccess("Project has been deleted successfully!!");
      }else
      {
        this.showError(data.message);
      }
      },
      error => {
          this.showError(error);
      });
          
  
}

onsuspend(proj,status)
{
  var isActive=proj.isActive;
  
  proj.isActive=status;

  this.projectService.addUpdateProject(proj).subscribe(
    data=>{
      if(data.result)
          {
          this.onReset();
          this.getAllProjects();
          if(isActive)
            this.showSuccess('Project Suspended successfully')
          else
            this.showSuccess('Project Activated successfully')
          }
       
          else
          {
            this.showError(data.message);
          }  
    },
    err=>{
      this.showError(err);
    }
  ) 
}
 onReset(){
  var editObj={
    'id': null,
    'projectName':"",
    'managerName': "",
    'managerId': null,
    'priority': 0, 
    'dateCheck': true,
    'startDtaeDisplay':this.getdateFormat( new Date()),
    'endDateDisplay':this.getdateFormat( new Date()),
    'isActive': true
  } 
 this.projectForm.setValue(editObj);
 this.projectForm.get('startDtaeDisplay').enable();
 this.projectForm.get('endDateDisplay').enable();
 this.addUpdateText="Add";
 }


 getdateFormat(dt:Date):string{  
   if(dt.getFullYear()==1970)
    return ""
   return dt.getFullYear()+"-"+  ("0"+(dt.getMonth()+1)).substr(("0"+(dt.getMonth())).length-2)
   +"-"+ ("0"+(dt.getDate())).substr(("0"+(dt.getDate())).length-2) 
 }

 checkChange(){
   if(!this.projectForm.controls['dateCheck'].value){
    this.projectForm.get('startDtaeDisplay').disable();
    this.projectForm.get('endDateDisplay').disable();
    this.projectForm.get('startDtaeDisplay').setValue(null);
    this.projectForm.get('endDateDisplay').setValue(null);
   }
   else{
    this.projectForm.get('startDtaeDisplay').enable();
    this.projectForm.get('endDateDisplay').enable();
    this.projectForm.get('startDtaeDisplay').setValue(this.getdateFormat(new Date()));
    this.projectForm.get('endDateDisplay').setValue(this.getdateFormat(new Date()));
   }
 }




  sortName(){
    this.orderBY="projectName";
    if(this.ascending)  // Ascending
   {
  //values.sort((a,b) => 0 - (a > b ? -1 : 1));
   this.projectLst=this.projectLstMaster.sort((a, b) => (a.projectName > b.projectName) ? -1 : 1)
   this.ascending=false;
   }else
  {
    // Descending
    this.projectLst=this.projectLstMaster.sort((a, b) => (a.projectName > b.projectName) ? 1 : -1)
    this.ascending=true;
  }
  }
  sortstartDate(){
    this.orderBY="startDate";
    if(this.ascending)  // Ascending
   {

   this.projectLst=this.projectLstMaster.sort((a, b) => (a.startDate > b.startDate) ? -1 : 1)
   this.ascending=false;
   }else
  {
    // Descending
    this.projectLst=this.projectLstMaster.sort((a, b) => (a.startDate > b.startDate) ? 1 : -1)
    this.ascending=true;
  }
  }
  sortendDate(){
    this.orderBY="endDate";
   
    if(this.ascending)  // Ascending
   {
   this.projectLst=this.projectLstMaster.sort((a, b) => (a.endDate > b.endDate) ? -1 : 1)
   this.ascending=false;
   }else
  {
    // Descending
    this.projectLst=this.projectLstMaster.sort((a, b) => (a.endDate > b.endDate) ? 1 : -1)
    this.ascending=true;
  }
  }
  sortPriority(){
    this.orderBY="priority";
    if(this.ascending)  // Ascending
   {
   this.projectLst=this.projectLstMaster.sort((a, b) => (a.priority > b.priority) ? -1 : 1)
   this.ascending=false;
   }else
  {
    // Descending
    this.projectLst=this.projectLstMaster.sort((a, b) => (a.priority > b.priority) ? 1 : -1)
    this.ascending=true;
  }
  }
  onKeyDown(event: any){
    if(event.charCode==13 || event.charCode==10){ 
      if(this.searchText && this.searchText.trim())
      {
      this.projectLst=this.projectLstMaster.filter(a=>a.managerName.toLowerCase().indexOf(this.searchText.toLowerCase()) >-1 || a.projectName.toLowerCase().indexOf(this.searchText.toLowerCase()) >-1 
      )
      }else
      {
        this.projectLst=this.projectLstMaster;
      }
    }
  }
  search(event: any){
    if(this.searchText && this.searchText.trim())
      {
      this.projectLst=this.projectLstMaster.filter(a=>a.managerName.toLowerCase().indexOf(this.searchText.toLowerCase()) >-1 || a.projectName.toLowerCase().indexOf(this.searchText.toLowerCase()) >-1 
      )
      }else
      {
        this.projectLst=this.projectLstMaster;
      }
  }


  //  onDelete(proj:Project){
  //   this.projectService.projectCheck(proj).subscribe(
  //     data=>{
  //       if(data){
  //         this.alert.error("Project is tagged with Task. Unable to delete!");
  //       }else{
  //         this.projectService.deleteProject(proj).subscribe(
  //           data=>{
  //             this.onReset();
  //             this.getAllProjects();
  //             this.alert.success("Project has been deleted successfully!!");
  //           }
  //         )
  //       }
  //     }
  //   );
//}
    
   
   
  
}
