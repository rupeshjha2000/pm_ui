import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectComponent } from './project.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserService, ProjectService, AlertService } from '../_services';
import { of } from 'rxjs';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MessageService } from 'primeng/api';


const mockUserService={
  getAll:()=> of({
    users: []
  })
}
const mockprojectService={
  
  getAllProjects:() => of({
    projects:[]
  })
  
}

const mockalertService={
  error:() =>{},
  success:() =>{}
}

describe('ProjectComponent', () => {
  let component: ProjectComponent;
  let fixture: ComponentFixture<ProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectComponent ],
      imports: [
        FormsModule, 
        ReactiveFormsModule,
        HttpClientModule,
        RouterModule,
        RouterTestingModule
      ],
      providers:[
        // {
        //   provide:UserService,
        //   useValue:mockUserService
        // },{
        //   provide: ProjectService,
        //   useValue:mockprojectService
        // },{
        //   provide: AlertService,
        //   useValue:mockalertService
        // },
        UserService,
        ProjectService,
        AlertService,
        MessageService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
