import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Project } from '../model/project';
import { User } from '../model/user';
import { Task } from '../model/task';
import { Observable, Subscription } from 'rxjs';
import { UserService, ProjectService, AlertService } from '../_services';
import { TaskService } from '../_services/task.service';
import { MessageService } from 'primeng/api';
import { SharedService } from '../_services/shared.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  taskLst:Task[];
  taskLstMaster:Task[];
  projectLst:Project[];
  projectLstMaster:Project[];
  userLst: User[]=[];
  userLstMaster: User[]=[];
  searchprojText:string;
  searchuserText:string;
  showtaskLst:boolean=true;
  public model: any;
  closeResult: string;
  searchtaskText:string;
  taskForm: FormGroup;
  message:string;
  title:string='Add';

//@Input() taskData: Observable<Task>;
private subscribeTaskEvents: Subscription;

  constructor(
    private readonly userService: UserService,
    private readonly projectService: ProjectService,
    private readonly formB: FormBuilder,
    private readonly taskService: TaskService,
    private readonly alert: AlertService,
    private messageService: MessageService,
    private sharedService: SharedService
  ) { 
   
  }
  
  ngOnInit() {
    this.buildForm();
    this.getAllProjects();
    this.getAllTask();
    this.getUsers();
    this.sharedService.sharedMessage.subscribe(message => this.message = message)
    this.sharedService.sharedTaskData.subscribe(
      data=>{
        if(data !=undefined && data.id==undefined)
        {
          var formatedDt=this.getFormattedDate(new Date());
          var enddateformatted=this.getFormattedDate(this.addDays(new Date(),1));
          data={
            'id': 0,
            'projectName': "",
            'projectId': 0,
            'taskName': "", 
            'parentTask': false,
            'parentTaskName':'',
            'parentTaskId':0,
            'priority': 0, 
            'isCompleted': false,  
            'startDate':new Date(),
            'endDate':this.addDays(new Date(),1),
            'userName':"",
            'userId':0
          } 
          this.title='Add'
        }
        else
        this.title='Edit'
        this.onEditTask(data);
      }
    );
  }
  
  ngOnDestroy() 
  {
    this.sharedService.taskEdit(new Task());
  }
  addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
}
  showSuccess(msg : string) {
    this.messageService.add({severity:'success', summary:'Success', detail:msg,closable:true,life:3000,sticky:false});
  }
  
  showError(msg : string) {
    this.messageService.add({severity:'error', summary:'Error', detail:msg,closable:true,life:3000,sticky:false});
  }

  buildForm() {
    var formatedDt=this.getFormattedDate(new Date());
    var enddateformatted=this.getFormattedDate(this.addDays(new Date(),1));
    this.taskForm = this.formB.group({
      'id': [null],
      'projectName':[''] ,//['', [Validators.required]],
      'projId': [0],
      'taskName': ['', [Validators.required, Validators.maxLength(100)]], 
      'ptaskcheck': [false],
      'priority': [0], 
      'ptask': [''],
      'ptaskid': [''],  
      'startDtaeDisplay':[formatedDt],
      'endDateDisplay':[enddateformatted],
      'User':["",[Validators.required]],
      'UserId':[0]
       
    })

    // this.taskForm.get('projectName').disable();
    // this.taskForm.get('User').disable();
    // this.taskForm.get('ptask').disable();
  }
  onResetClick(){
    var formatedDt=this.getFormattedDate(new Date());
    var enddateformatted=this.getFormattedDate(this.addDays(new Date(),1));
    var editObj={
      'id': 0,
      'projectName': "",
      'projId': 0,
      'taskName': "", 
      'ptaskcheck': false,
      'priority': 0, 
      'ptask': '',
      'ptaskid': '0',  
      'startDtaeDisplay':formatedDt,
      'endDateDisplay':enddateformatted,
      'User':"",
      'UserId':0
    } 
   this.taskForm.setValue(editObj);
   this.title='Add';
  }

  onSearchprojectKeyDown(event: any){
    if(event.charCode==13 || event.charCode==10){ 
     this.projectService.getAllProjects().subscribe(
       data=>{
        this.projectLst = data;
       }
     );
    }
  }

  getAllProjects(){
    
    this.projectService.getAllProjects().subscribe(
      data=>{ 
        this.projectLst= data;
        this.projectLstMaster= data;
      
        if(this.searchprojText && this.searchprojText.trim())
        {
        this.projectLst=this.projectLstMaster.filter(a=>a.managerName.toLowerCase().indexOf(this.searchprojText.toLowerCase()) >-1 || a.projectName.toLowerCase().indexOf(this.searchprojText.toLowerCase()) >-1 
        )
        }else
        {
          this.projectLst=this.projectLstMaster;
        }
        
      }
    )
  }
  onSearchProjectKeyPress(event: any){
    if(event.charCode==13 || event.charCode==10){ 
     
      this.searchProject(event);
    }
  }
  searchProject(event: any){
    this.getAllProjects();
   
  }
  projectSelect(proj:Project){ 
    this.searchprojText="";
    this.projectLst=this.projectLstMaster;
  
    this.taskForm.get('projectName').setValue(proj.projectName)
    this.taskForm.get('projId').setValue(proj.id);
    this.taskForm.get('ptask').setValue("")
    this.taskForm.get('ptaskid').setValue(0);
    //this.TaskResult=[]
  }

  getAllTask(){
    
    this.taskService.getTasks().subscribe(
      data=>{  
       this.taskLstMaster=data;
       this.taskLst=data;
       if(this.taskForm.controls['projId'].value)
          this.taskLst = this.taskLstMaster.filter(a=>a.projectId===this.taskForm.controls['projId'].value);
       this.showtaskLst=this.taskLst.length>0?true:false;
      }
    )
  }
  searchTask(event: any){
    this.getAllTask();
    if(this.searchtaskText && this.searchtaskText.trim())
      {
      this.taskLst=this.taskLstMaster.filter(a=>a.projectId===this.taskForm.controls['projId'].value && (a.taskName.toLowerCase().indexOf(this.searchprojText.toLowerCase()) >-1 || a.projectName.toLowerCase().indexOf(this.searchprojText.toLowerCase()) >-1) 
      )
      }else
      {
        this.taskLst=this.taskLstMaster.filter(a=>a.projectId===this.taskForm.controls['projId'].value);
      }
      this.showtaskLst=this.taskLst.length>0?true:false;
  }

  onSearchTaskKeyDown(event: any){
    if(event.charCode==13 ||event.charCode==10){ 
      
      this.searchTask(event);
    }
  }

  taskSelect(tsk:Task){
    this.taskForm.get('ptask').setValue(tsk.taskName)
    this.taskForm.get('ptaskid').setValue(tsk.id);
  }
  
  getUsers(){
    this.userService.getAll().subscribe(
      data=>{ 
        this.userLst= data;
        this.userLstMaster=data;
       if(this.searchuserText && this.searchuserText.trim())
        {
        this.userLst=this.userLstMaster.filter(a=>a.firstName.toLowerCase().indexOf(this.searchuserText.toLowerCase()) >-1 || a.lastName.toLowerCase().indexOf(this.searchuserText.toLowerCase()) >-1 || a.empId.indexOf(this.searchuserText) >-1)
        }else
        {
          this.userLst=this.userLstMaster;
        }
      }
    )
  }

  onSearchUserKeyDown(event: any){
    if(event.charCode==13 ||event.charCode==10){ 
      
      this.searchUser();
    }
  }

searchUser()
 {
   this.getUsers();
  
}

userselect(usr:User){
  this.taskForm.get('User').setValue(usr.firstName + " " + usr.lastName + "(" +usr.empId + ")" )
    this.taskForm.get('UserId').setValue(usr.id);
}

  onSubmitTask(){ 
    let tsk=<Task>  this.taskForm.value;
    tsk.projectName =this.taskForm.controls['projectName'].value;
    tsk.projectId = this.taskForm.controls['projId'].value;
    tsk.taskName = this.taskForm.controls['taskName'].value;
    tsk.parentTask = this.taskForm.controls['ptaskcheck'].value;
    tsk.priority = this.taskForm.controls['priority'].value;
    tsk.parentTaskName = this.taskForm.controls['ptask'].value;
    tsk.parentTaskId = this.taskForm.controls['ptaskid'].value? this.taskForm.controls['ptaskid'].value:0;
    tsk.startDate = new Date(this.taskForm.controls['startDtaeDisplay'].value)
    tsk.endDate = new Date(this.taskForm.controls['endDateDisplay'].value)
    tsk.userId = this.taskForm.controls['UserId'].value;
    tsk.userName = this.taskForm.controls['User'].value;


 
    this.taskService.addUpdateTask(tsk).subscribe(
      data=>{
        this.onResetClick();
        this.showSuccess("Task Saved Successfully");
      },
      err=>{
        this.showError(err.error);
      }
    );
 }
 
 getFormattedDate(dt:Date):string{  
  if(dt.getFullYear()==1970)
   return ""
  return dt.getFullYear()+"-"+  ("0"+(dt.getMonth()+1)).substr(("0"+(dt.getMonth())).length-2)
  +"-"+ ("0"+(dt.getDate())).substr(("0"+(dt.getDate())).length-2) 
}


  onEditTask(tsk:Task){
    console.log("Edited Task",tsk);
    var editObj={
      'id': tsk.id,
      'projectName': tsk.projectName,
      'projId': tsk.projectId,
      'taskName': tsk.taskName,
      'ptaskcheck': tsk.parentTask,
      'priority': tsk.priority,
      'ptask': tsk.parentTaskName,
      'ptaskid': tsk.parentTaskId,
      'startDtaeDisplay':this.getFormattedDate(new Date(tsk.startDate)),
      'endDateDisplay':this.getFormattedDate(new Date( tsk.endDate)),
      'User':tsk.userName,
      'UserId':tsk.userId
    }
    this.taskForm.setValue(editObj);
    
  }

  
  checkChange(){
    if(this.taskForm.controls['ptaskcheck'].value){ 
      this.taskForm.get('ptask').setValue("")
      this.taskForm.get('ptaskid').setValue(0);
      this.taskForm.get('startDtaeDisplay').disable();
      this.taskForm.get('endDateDisplay').disable();
      this.taskForm.get('priority').disable();
      // this.taskForm.get('startDtaeDisplay').setValue(null);
      // this.taskForm.get('endDateDisplay').setValue(null);
    }else
    {
      var formatedDt=this.getFormattedDate(new Date());
      var enddateformatted=this.getFormattedDate(this.addDays(new Date(),1));

      this.taskForm.get('startDtaeDisplay').enable();
      this.taskForm.get('endDateDisplay').enable();
      this.taskForm.get('priority').enable();
      // this.taskForm.get('startDtaeDisplay').setValue(formatedDt);
      // this.taskForm.get('endDateDisplay').setValue(enddateformatted);
    }
 
  }

}
