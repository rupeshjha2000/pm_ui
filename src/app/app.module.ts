import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AlertService,  UserService } from './_services';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AlertComponent } from './_directives';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import {ToastModule} from 'primeng/toast';
import {ListboxModule} from 'primeng/listbox';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MessageService } from 'primeng/api';
import {DataViewModule} from 'primeng/dataview';
import {DropdownModule} from 'primeng/dropdown';
import {PanelModule} from 'primeng/panel';
import {CalendarModule} from 'primeng/calendar';
import { UserComponent } from './user/user.component';
import { ProjectComponent } from './project/project.component';
import { TaskComponent } from './task/task.component';
import { ProjectmanagerComponent } from './projectmanager/projectmanager.component';
import { ViewtaskComponent } from './viewtask/viewtask.component';
import { SharedService } from './_services/shared.service';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    UserComponent,
    ProjectComponent,
    TaskComponent,
    ProjectmanagerComponent,
    ViewtaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    InputTextModule,
    DialogModule,
    ButtonModule,
    TableModule,
    BrowserAnimationsModule,
    ToastModule,
    ListboxModule,
    FormsModule,
    DataViewModule,
    DropdownModule,
    PanelModule,
    CalendarModule
   
  ],
  providers: [
    AlertService,
    UserService,
    MessageService,
    SharedService
    ],
  bootstrap: [AppComponent],
})
export class AppModule { }
