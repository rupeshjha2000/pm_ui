import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { Task } from './model/task';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Project Manager UI';
    
}
