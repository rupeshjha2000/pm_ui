import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { ProjectComponent } from './project/project.component';
import { TaskComponent } from './task/task.component';


const appRoutes: Routes = [
        
];

export const routing = RouterModule.forRoot(appRoutes);
