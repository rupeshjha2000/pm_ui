import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/app/model/user';
import { AlertService, UserService } from '../_services';
import { MessageService } from 'primeng/api';
import { first } from 'rxjs/operators';
import { $ } from 'protractor';
import {  SortUtil } from '../utility/sortutil.utility';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  userForm: FormGroup;
  userLst: User[]=[];
  userLstMaster: User[]=[];
  searchText:string ="";
  orderBY:string="id";
  recordCount:number=0;
  pageNo:number=0;
  ascending:boolean=true;
  addUpdateUserText="Add";
  constructor(
    private readonly userService: UserService,
    private readonly alert: AlertService,
    private messageService: MessageService,
    private readonly formb: FormBuilder
  ) { 
    
  }

  ngOnInit() {
    this.buildForm();
    this.getUsers(); 
  }

  buildForm() {
    this.userForm = this.formb.group({
      'id': [null], 
      'empId': ['', [Validators.required, Validators.maxLength(20)]], 
      'firstName': ['', [Validators.required, Validators.maxLength(50)]],
      'lastName': ['', [Validators.required, Validators.maxLength(50)]],
      
      
    })
  }
  showSuccess(msg : string) {
    this.messageService.add({severity:'success', summary:'Success', detail:msg,closable:true,life:3000,sticky:false});
  }
  
  showError(msg : string) {
    this.messageService.add({severity:'error', summary:'Error', detail:msg,closable:true,life:3000,sticky:false});
  }
  onReset(){
    let newUsr:User={
      id:null,
      empId:'',
      firstName:'',
      lastName:''
    };
    this.userForm.setValue(newUsr);
    this.addUpdateUserText="Add";
  }
  
  getUsers(){
    this.userService.getAll().subscribe(
      data=>{ 
        this.userLst= data;
       this.userLstMaster=data;
      }
    )
  }
  // convenience getter for easy access to form fields
  get f() { return this.userForm.controls; }

  onSubmit() {
      // stop here if form is invalid
      if (this.userForm.invalid) {
          return;
      }

      var userData =this.userForm.value;
      if(userData.id==null)
      {
      this.userService.createUser(this.userForm.value)
      .subscribe(data => {  
            if(data.result)
            {
            this.onReset();
            this.getUsers();  
            this.showSuccess('success')
            
            }else
            {
              this.showError(data.message);
            }  
          },
          error => {
              this.showError(error);
          });     
        }
        else
        {
          this.userService.update(this.userForm.value)
        .subscribe(data => {  
          if(data.result)
          {
            this.showSuccess('user updateed successfully')
            this.getUsers();    
            this.onReset();
          }else
          {
            this.showError(data.message);
          }
        },
          error => {
              this.showError(error);
          });   

          
        }
  }

  onEdit(usr:User){
     this.userForm.setValue(usr);
     this.addUpdateUserText="Edit";
     let inputField: HTMLElement = <HTMLElement>document.querySelectorAll('.form-control')[0];
      inputField && inputField.focus();
    }
   
    
  onDelete(usr:User){
 
    this.userService.deleteUser(usr).subscribe(
        data=>{
          if(data.result)
          {
          this.onReset();
          this.getUsers();  
          this.showSuccess("delete successfully");
          }else
          {
            this.showError(data.message);
          }
        },
        error => {
            this.showError(error);
        });
      
};


 
search()
 {
  if(this.searchText && this.searchText.trim())
  {
  this.userLst=this.userLstMaster.filter(a=>a.firstName.toLowerCase().indexOf(this.searchText.toLowerCase()) >-1 || a.lastName.toLowerCase().indexOf(this.searchText.toLowerCase()) >-1 || a.empId.indexOf(this.searchText) >-1)
  }else
  {
    this.userLst=this.userLstMaster;
  }
}
OnEnterKeyPressed(event: any){
  if(event.charCode==10 ||event.charCode==13){ 
    if(this.searchText && this.searchText.trim())
    {
    this.userLst=this.userLstMaster.filter(a=>a.firstName.toLowerCase().indexOf(this.searchText.toLowerCase()) >-1 || a.lastName.toLowerCase().indexOf(this.searchText.toLowerCase()) >-1 || a.empId.indexOf(this.searchText) >-1)
    }else
    {
      this.getUsers();
    }
  }
}

  
 
 sortFirstName(){
   this.orderBY="firstName";
   //if(this.ascending)  // Ascending
   //{
  
  //  this.userLst=this.userLstMaster.sort((a, b) => (a.firstName > b.firstName) ? -1 : 1)
  //  this.ascending=false;
  //  }else// Descending
  // {
    
  //   this.userLst=this.userLstMaster.sort((a, b) => (a.firstName > b.firstName) ? 1 : -1)
  //   this.ascending=true;
  // }

  SortUtil.sortByProperty(this.userLstMaster,"firstName",this.ascending?"ASC": "DESC");
   this.ascending=this.ascending?false:true;
 }
 sortLastName(){
   this.orderBY="lastName";
   
   SortUtil.sortByProperty(this.userLstMaster,"lastName",this.ascending?"ASC": "DESC");
   this.ascending=this.ascending?false:true;

  //  if(this.ascending)  // Ascending
  //  {
  //  this.userLst=this.userLstMaster.sort((a, b) => (a.lastName > b.lastName) ? -1 : 1)
  //  this.ascending=false;
  //  }else
  // {
  //   // Descending
  //   this.userLst=this.userLstMaster.sort((a, b) => (a.lastName > b.lastName) ? 1 : -1)
  //   this.ascending=true;
  // }
 }
 sortEmpId(){
   this.orderBY="empId"
    
  //  SortUtilService.sortByProperty(this.userLstMaster,"empId",this.ascending?"ASC": "DESC");
  //  this.ascending=this.ascending?false:true;

   if(this.ascending)  // Ascending
   {
   this.userLst=this.userLstMaster.sort((a, b) => (a.empId > b.empId) ? -1 : 1)
   this.ascending=false;
   }else
  {
    // Descending
    this.userLst=this.userLstMaster.sort((a, b) => (parseInt(a.empId) > parseInt(b.empId)) ? 1 : -1)
    this.ascending=true;
  }
 }

}
