﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

import { map } from 'rxjs/operators';
import { User } from '../model/user';
import { Observable, forkJoin } from 'rxjs';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() :Observable<any[]>{
        return this.http.get<any[]>(`${environment.ApiUrl}/user`);
    }

    getById(id: number) {
        return this.http.get(`${environment.ApiUrl}/user/` + id);
    }

    createUser(user: User) :Observable<any>{
        user.id=0;
        return this.http.post<any>(`${environment.ApiUrl}/user`, user);
    }

    update(user: User) :Observable<any>{
        return this.http.put<any>(`${environment.ApiUrl}/user/` + user.id, user);
        // .pipe(map(data => {
        //      return data;
        // }));
    }

   
    deleteUser(usr:User):Observable<any>{
        return this.http.post<any>(environment.ApiUrl + '/user/delete/'+usr.id, null)
    }

    userCheck(usr:User): Observable<any[]>{
        let response1 = this.http.get(environment.ApiUrl + 'task/userexits/'+usr.id);
        let response2 = this.http.get(environment.ApiUrl + 'project/userexits/'+usr.id); 
      
        return forkJoin([response1, response2]);
    }
}