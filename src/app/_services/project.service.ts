import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Project } from '../model/project';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn:'root'
})
export class ProjectService{

    constructor(
        private readonly http: HttpClient
    ){
    }

    addUpdateProject(proj:Project):Observable<any>{
        if(proj.id){
            return this.http.put<any>(`${environment.ApiUrl}/project/` + proj.id, proj);
        }else{
            proj.id=0;
        return this.http.post<any>(environment.ApiUrl + '/project', proj) ;
        }
    }
    getAllProjects():Observable<Project[]>{
        return this.http.get<Project[]>(`${environment.ApiUrl}/project`);
    }

    deleteProject(proj:Project):Observable<any>{
        return this.http.post<any>(environment.ApiUrl + '/project/delete/'+proj.id, null)
    }

    projectCheck(proj:Project): Observable<boolean>{
       return this.http.get<boolean>(environment.ApiUrl + 'task/projectxits/'+proj.id);         
    }

    projectCount(isCompleted:boolean):Observable<number>{
        return this.http.get<number>(environment.ApiUrl + 'project/projCount/'+isCompleted);         
    }
    projectoverDueCount():Observable<number>{
        return this.http.get<number>(environment.ApiUrl + 'project/overdueproj');         
    }
}
