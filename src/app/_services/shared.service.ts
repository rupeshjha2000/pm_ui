import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { Task } from '../model/task';

@Injectable()
export class SharedService {

  private message = new BehaviorSubject('First Message');
  sharedMessage = this.message.asObservable();

  private taskData = new BehaviorSubject<Task>(new Task());
  sharedTaskData = this.taskData.asObservable();
 
  constructor() { }

  nextMessage(message: string) {
    this.message.next(message)
  }
  
  taskEdit(task: any) {
    this.taskData.next(task)
  }
}