import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Task } from '../model/task';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn:'root'
})
export class TaskService{
    constructor(
        private readonly http: HttpClient
        ) {}

    getTasks():Observable<any>{
        return this.http.get<any>(`${environment.ApiUrl}/task`)
    }

    addUpdateTask(task:Task):Observable<any>{
        if(task.id){
            return this.http.put<any>(`${environment.ApiUrl}/task/` + task.id, task);
        }else{
            task.id=0;
            return this.http.post<any>(environment.ApiUrl + '/task', task) 
         
        }
    }

    getProjectTasks(projectId: string):Observable<any>{
        return this.http.post<any>(environment.ApiUrl + '/task/getprojectTasks/'+projectId,null)
    }
    updateTaskToComplete(task: any):Observable<any>{
        
        return this.http.post<any>(environment.ApiUrl + '/task/updateTaskToComplete/'+task.projectId,task);
    }
    


}