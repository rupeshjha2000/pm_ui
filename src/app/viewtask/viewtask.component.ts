import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Task } from '../model/task';
import { TaskService } from '../_services/task.service';
import { AlertService, ProjectService } from '../_services';
import { Project } from '../model/project';
import { MessageService } from 'primeng/api';
import { SharedService } from '../_services/shared.service';
import { Router } from '@angular/router';
import { SortUtil } from '../utility/sortutil.utility';

@Component({
  selector: 'app-viewtask',
  templateUrl: './viewtask.component.html',
  styleUrls: ['./viewtask.component.css']
})
export class ViewtaskComponent implements OnInit {
  projectLst:Project[];
  projectLstMaster:Project[];
  searchprojText:string;
  searchText:string=""
  prodjId:number=0
  showtaskLst:boolean=true;
  ascending:boolean=true;
  pageNo:number=1;
  orderBY:string="id";
  taskLst:Task[];
  taskLstMaster:Task[];
  recordCount:number=0;
  message:string;
  taskData:Task;
  //@Output() performEdit = new EventEmitter<Task>();
  constructor(
    private router: Router,
    private readonly taskService: TaskService,
    private readonly projectService: ProjectService,
    private readonly alert: AlertService,
    private messageService: MessageService,
    private sharedService: SharedService
  ) { }

  ngOnInit() {
    this.getAllTAsk();
    this.getAllProjects();
    this.sharedService.sharedMessage.subscribe(message => this.message = message)
    this.sharedService.sharedTaskData.subscribe(taskData => this.taskData = taskData)
    
  }
  

  onSearchKeyDown(event: any){
    if(event.charCode==13){ 
    this.getAllTAsk();
    }
  }

  getAllTAsk(){
    
    this.taskService.getTasks().subscribe(
      data=>{ 
        this.taskLst= data;
        this.taskLstMaster=data;
        this.showtaskLst=data.length>0?true:false;
      }
    )
  }
  onEdit(taskData:Task){
    //this.performEdit.emit(taskData);
     this.newMessage(taskData);
     this.router.navigate(['./task']);
  }
  newMessage(taskData:Task) {
    this.sharedService.nextMessage("Second Message" + new Date())
    this.sharedService.taskEdit(taskData);
  }
  getProjectTask(projectId:any){
    
    this.taskService.getProjectTasks(projectId).subscribe(
      data=>{ 
        this.taskLst= data;
        this.taskLstMaster=data;
        this.showtaskLst=data.length>0?true:false;
      }
    )
  }
  getAllProjects(){
    
    this.projectService.getAllProjects().subscribe(
      data=>{ 
        this.projectLst= data;
        this.projectLstMaster= data;
      
      }
    )
  }


  onSearchProjectKeyPress(event: any){
    if(event.charCode==13 || event.charCode==10){ 
     
      this.searchProject(event);
    }
  }
  searchProject(event: any){
    //this.getAllProjects();
    //this.showtaskLst=true;
    if(this.searchprojText && this.searchprojText.trim())
      {
      this.projectLst=this.projectLstMaster.filter(a=>a.managerName.toLowerCase().indexOf(this.searchprojText.toLowerCase()) >-1 || a.projectName.toLowerCase().indexOf(this.searchprojText.toLowerCase()) >-1 
      )
      }else
      {
        this.projectLst=this.projectLstMaster;
      }
  }
  projectSelect(proj:Project){ 
    this.searchprojText="";
    this.projectLst=this.projectLstMaster;
    this.searchText=proj.projectName;
    this.searchTask(proj);
  }
  searchTask(proj:any){
    this.getProjectTask(proj.id);
    //this.taskLst=this.taskLstMaster.filter(a=>a.projectId===proj.projectId);
    
  }
    

  

    sortStartDate(){
      this.orderBY="startDate";
    //   if(this.ascending)  // Ascending
    //   {
    //   this.taskLst=this.taskLstMaster.sort((a, b) => (a.startDate > b.startDate) ? -1 : 1)
    //   this.ascending=false;
    //   }else// Descending
    //  {
    //    this.taskLst=this.taskLstMaster.sort((a, b) => (a.startDate > b.startDate) ? 1 : -1)
    //    this.ascending=true;
    //  }
    SortUtil.sortByProperty(this.taskLst,"startDate",this.ascending?"ASC": "DESC");
    this.ascending=this.ascending?false:true;
      
    }
    sortEndDate(){
      this.orderBY="endDate";
      // if(this.ascending)  // Ascending
      // {
      // this.taskLst=this.taskLstMaster.sort((a, b) => (a.endDate > b.endDate) ? -1 : 1)
      // this.ascending=false;
      // }else// Descending
      // {
      //   this.taskLst=this.taskLstMaster.sort((a, b) => (a.endDate > b.endDate) ? 1 : -1)
      //   this.ascending=true;
      // }
      SortUtil.sortByProperty(this.taskLst,"endDate",this.ascending?"ASC": "DESC");
      this.ascending=this.ascending?false:true;
    }

    sortPriority(){
      this.orderBY="priority";
      if(this.ascending)  // Ascending
      {
      this.taskLst=this.taskLstMaster.sort((a, b) => (a.priority > b.priority) ? -1 : 1)
      this.ascending=false;
      }else// Descending
     {
       this.taskLst=this.taskLstMaster.sort((a, b) => (a.priority > b.priority) ? 1 : -1)
       this.ascending=true;
     }
    }
    sortCompleted(){
      this.orderBY="isCompleted";
      if(this.ascending)  // Ascending
      {
      this.taskLst=this.taskLstMaster.sort((a, b) => (a.isCompleted > b.isCompleted) ? -1 : 1)
      this.ascending=false;
      }else// Descending
     {
       this.taskLst=this.taskLstMaster.sort((a, b) => (a.isCompleted > b.isCompleted) ? 1 : -1)
       this.ascending=true;
     }
    }
    showSuccess(msg : string) {
      this.messageService.add({severity:'success', summary:'Success', detail:msg,closable:true,life:3000,sticky:false});
    }
    
    showError(msg : string) {
      this.messageService.add({severity:'error', summary:'Error', detail:msg,closable:true,life:3000,sticky:false});
    }
  
    onComplete(tsk:Task){
     
      this.taskService.updateTaskToComplete(tsk).subscribe(
        data=>{
          if(!data.result){
            this.showError(data.message);
          }else{
            this.taskLst=data.projectTask;
            this.taskLstMaster=data.projectTask
            this.showSuccess(data.message);
          }
        }
      )
    }

}
