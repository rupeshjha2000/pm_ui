import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewtaskComponent } from './viewtask.component';
import { of } from 'rxjs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertService } from '../_services';
import { TaskService } from '../_services/task.service';
import { MessageService } from 'primeng/api';
import { SharedService } from '../_services/shared.service';
import { RouterTestingModule } from '@angular/router/testing';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

const mockalertService={
  error:() =>{},
  success:() =>{}
}

const mockTaskService={
  getTasks:() =>of({
    tasks:[],
    count:0
  })
}


describe('ViewtaskComponent', () => {
  let component: ViewtaskComponent;
  let fixture: ComponentFixture<ViewtaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewtaskComponent ],
      imports:[
        FormsModule, 
        ReactiveFormsModule,
        HttpClientModule,
        RouterModule,
        RouterTestingModule
      ],
      providers:[
        {
          provide: AlertService,
          useValue:mockalertService
        }, {
          provide: TaskService,
          useValue: mockTaskService
        },
        MessageService,
        SharedService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewtaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
